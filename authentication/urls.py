from django.urls import path

from knox import views as knox_views

from authentication.views import ValidatePhoneSendOTP, ValidateOTP, Register, LoginAPI



urlpatterns = [
    path('verify-phone/', ValidatePhoneSendOTP.as_view()),
    path('verify-phone-otp/', ValidateOTP.as_view()),
    path('register/', Register.as_view()),
    path('login/', LoginAPI.as_view()),
    path('logout/', knox_views.LogoutView.as_view()),
]
